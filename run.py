import csv
import xml.etree.ElementTree as ET

tree = ET.parse('testfileMod.xml')

# get root element
root = tree.getroot()

childVal = {}

# find all children on this node
children = root[0][0][0].findall('.')

for child in children:
    ET.dump(child)

    # specify field names for csv file
    fields = ['200','12345678901','E1','E1','E1','N1','HGLMET501','KWH','30']

# write to csv file
with open('outputCSV', 'w') as csvfile:
    # csv dictionWriter defn
    writer = csv.DictWriter(csvfile, fieldnames=fields)

    # write header values
    writer.writeheader()

    # writing data rows
    writer.writerows(childVal)
