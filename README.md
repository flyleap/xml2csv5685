
## What was done in 4 hours
Using Python: Am able to write header values to csv file (outputCSV.xml) already.  
Comments in code explain current steps.

## What did not get done

> Current block: Unable to create proper dictonary with xmlchild values yet  

Have never written tests in Python, so that didnt get done

## What could possibly be done with an extra 24-36 hours
Usually prefer C# > python. Use C# XDocument/LINQ and automated tests  
C# XDocument is more barebones - required more time to understand  
Either: Use C# XDoc/LINQ XML parser and write automated tests  
    
OR: Keep going with Python direction but write automated tests
  
  
#### input: testfileMod.xml
#### output: ouputCSV.csv